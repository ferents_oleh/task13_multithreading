package com.epam.service;

import com.epam.model.Counter;

import java.util.concurrent.Callable;

public class SynchronizeTask implements Callable<Integer> {
    private Counter counter;

    public SynchronizeTask(Counter counter) {
        this.counter = counter;
    }

    @Override
    public Integer call() {
        for (int i = 0; i < 5; ++i) {
            counter.methodA();
        }
        for (int i = 0; i < 3; ++i) {
            counter.methodB();
        }
        for (int i = 0; i < 7; ++i) {
            counter.methodC();
        }
        return counter.getCount();
    }
}
