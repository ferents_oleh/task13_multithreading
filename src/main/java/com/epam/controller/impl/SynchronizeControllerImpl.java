package com.epam.controller.impl;

import com.epam.controller.SynchronizeController;
import com.epam.model.Counter;
import com.epam.service.SynchronizeTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SynchronizeControllerImpl implements SynchronizeController {
    @Override
    public void doSynchronize() {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Future<Integer> future = executorService.submit(new SynchronizeTask(new Counter()));
        Future<Integer> future1 = executorService.submit(new SynchronizeTask(new Counter()));
        Future<Integer> future2 = executorService.submit(new SynchronizeTask(new Counter()));
        try {
            System.out.println("1 " + future.get());
            System.out.println("2 " + future1.get());
            System.out.println("3 " + future2.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
