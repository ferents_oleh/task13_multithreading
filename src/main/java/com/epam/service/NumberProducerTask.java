package com.epam.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class NumberProducerTask implements Runnable {
    private BlockingQueue<Integer> deque;

    private int poisonPill;

    private int poisonPillPerProducer;

    public NumberProducerTask(BlockingQueue<Integer> deque, int poisonPill, int poisonPillPerProducer) {
        this.deque = deque;
        this.poisonPill = poisonPill;
        this.poisonPillPerProducer = poisonPillPerProducer;
    }

    @Override
    public void run() {
        try {
            generateNumbers();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void generateNumbers() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            int value = ThreadLocalRandom.current().nextInt(10);
            deque.put(value);
            System.out.println("Put - " + value);
        }
        for (int j = 0; j < poisonPillPerProducer; j++) {
            System.out.println("Poison pill - " + poisonPill);
            deque.put(poisonPill);
        }
    }
}
