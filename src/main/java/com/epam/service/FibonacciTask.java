package com.epam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciTask implements Callable<Integer> {
    private int n;

    private List<Integer> fibonacciNums = new ArrayList<>();

    public List<Integer> getFibonacciNums() {
        return fibonacciNums;
    }

    public FibonacciTask(int n) {
        this.n = n;
    }

    @Override
    public Integer call() {
        int f1 = 0;
        int f2 = 1;
        fibonacciNums.clear();

        for (int i = 1; i <= n; i++) {
            fibonacciNums.add(f2);
            int next = f1 + f2;
            f1 = f2;
            f2 = next;
        }
        return getFibonacciNums()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
