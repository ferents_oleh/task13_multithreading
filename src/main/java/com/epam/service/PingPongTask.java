package com.epam.service;

import java.util.concurrent.locks.Lock;

public class PingPongTask implements Runnable {
    private String word;

    private Lock lock;

    public PingPongTask(String word, Lock lock) {
        this.word = word;
        this.lock = lock;
    }

    @Override
    public void run() {
        System.out.println(word);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lock.notify();
        try {
            lock.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
