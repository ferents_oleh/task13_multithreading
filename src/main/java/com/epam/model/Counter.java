package com.epam.model;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Counter {
    private Integer count = 0;

    private Lock lock = new ReentrantLock();

    public Integer getCount() {
        return count;
    }

    public void methodA() {
        lock.lock();
        try {
            count++;
        } finally {
            lock.unlock();
        }
    }

    public void methodB() {
        lock.lock();
        try {
            count++;
        } finally {
            lock.unlock();
        }
    }

    public void methodC() {
        lock.lock();
        try {
            count++;
        } finally {
            lock.unlock();
        }
    }
}
