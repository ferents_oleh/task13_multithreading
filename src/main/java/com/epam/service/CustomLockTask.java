package com.epam.service;

public class CustomLockTask implements Runnable{
    private CustomLock customLock;

    private int value;

    public CustomLockTask(CustomLock customLock) {
        this.customLock = customLock;
        this.value = 3;
    }

    @Override
    public void run() {
        System.out.println("Waiting for: " + Thread.currentThread().getName());

        customLock.lock();

        if (value > 0) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            value--;
            System.out.println("Value changed by: " + Thread.currentThread().getName());
        }
        System.out.println("Finish by: " + Thread.currentThread().getName());
        customLock.unlock();
    }
}
