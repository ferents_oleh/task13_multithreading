package com.epam.controller.impl;

import com.epam.controller.NumberController;
import com.epam.service.NumberConsumerTask;
import com.epam.service.NumberProducerTask;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class NumberControllerImpl implements NumberController {
    private BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

    private int poisonPill = Integer.MAX_VALUE;

    private final int PRODUCER_COUNT = 4;

    private final int CONSUMER_COUNT = Runtime.getRuntime().availableProcessors();

    private int poisonPillPerProducer = CONSUMER_COUNT / PRODUCER_COUNT;

    private int mod = CONSUMER_COUNT % PRODUCER_COUNT;

    @Override
    public void runProducerConsumer() {
        for (int i = 0; i < PRODUCER_COUNT; ++i) {
            new Thread(new NumberProducerTask(queue, poisonPill, poisonPillPerProducer)).start();
        }

        for (int i = 0; i < CONSUMER_COUNT; ++i) {
            new Thread(new NumberConsumerTask(queue, poisonPill)).start();
        }

        new Thread(new NumberProducerTask(queue, poisonPill, poisonPillPerProducer + mod)).start();
    }
}
