package com.epam.controller.impl;

import com.epam.controller.FibonacciController;
import com.epam.service.FibonacciTask;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibonacciControllerImpl implements FibonacciController {
    @Override
    public void calcFibonacci() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        List<Future<Integer>> futureList = List.of(
                executorService.submit(new FibonacciTask(15)),
                executorService.submit(new FibonacciTask(5)),
                executorService.submit(new FibonacciTask(8))
                );
        for (Future<Integer> future : futureList) {
            System.out.println(future.get());
        }
    }
}
