package com.epam.controller;

import java.util.concurrent.ExecutionException;

public interface FibonacciController {
    void calcFibonacci() throws ExecutionException, InterruptedException;
}
