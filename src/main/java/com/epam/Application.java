package com.epam;

import com.epam.controller.impl.*;
import com.epam.service.CustomLockImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(
                new PingPongControllerImpl(),
                new FibonacciControllerImpl(),
                new SleepControllerImpl(),
                new SynchronizeControllerImpl(),
                new NumberControllerImpl(),
                new CustomLockControllerImpl(new CustomLockImpl())
        );
    }
}
