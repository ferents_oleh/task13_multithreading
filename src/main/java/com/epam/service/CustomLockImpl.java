package com.epam.service;

public class CustomLockImpl implements CustomLock {
    private int lockHoldCount = 0;

    private long currentIdThread;

    @Override
    public synchronized void lock() {
        if (lockHoldCount == 0) {
            lockHoldCount++;
            currentIdThread = Thread.currentThread().getId();
        } else if (lockHoldCount > 0 && currentIdThread == Thread.currentThread().getId()) {
            lockHoldCount++;
        } else {
            try {
                wait();
                lockHoldCount++;
                currentIdThread = Thread.currentThread().getId();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void unlock() {
        if (lockHoldCount == 0) {
            throw new IllegalMonitorStateException();
        }

        lockHoldCount--;

        if (lockHoldCount == 0) {
            notify();
        }
    }

    @Override
    public synchronized boolean tryLock() {
        if (lockHoldCount == 0) {
            lock();
            return true;
        }
        return false;
    }
}
