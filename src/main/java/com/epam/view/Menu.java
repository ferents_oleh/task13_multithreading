package com.epam.view;

import com.epam.controller.*;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private PingPongController pingPongController;

    private FibonacciController fibonacciController;

    private SleepController sleepController;

    private SynchronizeController synchronizeController;

    private NumberController numberController;

    private CustomLockController customLockController;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu(PingPongController pingPongController,
                FibonacciController fibonacciController,
                SleepController sleepController,
                SynchronizeController synchronizeController,
                NumberController numberController,
                CustomLockController customLockController) {
        this.pingPongController = pingPongController;
        this.fibonacciController = fibonacciController;
        this.sleepController = sleepController;
        this.synchronizeController = synchronizeController;
        this.numberController = numberController;
        this.customLockController = customLockController;

        Menu menu = new Menu("Main menu", "");
        menu.putAction("Start ping pong", pingPongController::startPingPong);
        menu.putAction("Fibonacci", () -> {
            try {
                fibonacciController.calcFibonacci();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        menu.putAction("Show sleep time", sleepController::printSleepTime);
        menu.putAction("Synchronize", synchronizeController::doSynchronize);
        menu.putAction("Run blocking queue", numberController::runProducerConsumer);
        menu.putAction("Use custom reentrant lock", customLockController::useCustomLock);

        menu.putAction("Exit", () -> System.exit(0));

        activateMenu(menu);
    }


    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
