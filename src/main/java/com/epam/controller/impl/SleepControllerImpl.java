package com.epam.controller.impl;

import com.epam.controller.SleepController;
import com.epam.service.SleepTask;

import java.time.LocalTime;
import java.util.Scanner;
import java.util.concurrent.*;

public class SleepControllerImpl implements SleepController {
    @Override
    public void printSleepTime() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter count of tasks: ");
        int count = scanner.nextInt();
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(count);
        System.out.println("The start time is: " + LocalTime.now());
        for (int i = 0; i < count; ++i) {
            ScheduledFuture<?> future = executorService.schedule(new SleepTask(), i, TimeUnit.SECONDS);
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println("The end time is: " + LocalTime.now());
    }
}
