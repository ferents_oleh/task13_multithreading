package com.epam.service;

import java.util.concurrent.BlockingQueue;

public class NumberConsumerTask implements Runnable {
    private BlockingQueue<Integer> queue;

    private int poisonPill;

    public NumberConsumerTask(BlockingQueue<Integer> queue, int poisonPill) {
        this.queue = queue;
        this.poisonPill = poisonPill;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int number = queue.take();
                if (number == poisonPill) {
                    return ;
                }
                System.out.println(Thread.currentThread().getName() + " result - " + number);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
