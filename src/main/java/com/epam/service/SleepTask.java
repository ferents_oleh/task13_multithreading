package com.epam.service;

import java.time.LocalTime;
import java.util.Random;

public class SleepTask implements Runnable {
    @Override
    public void run() {
        int sleep = new Random().nextInt(10) + 1;
        LocalTime localTime = LocalTime.now();
        int timeDifference;
        try {
            Thread.sleep(sleep * 1000);
            timeDifference = LocalTime.now().getSecond() - localTime.getSecond();
            System.out.println("Sleep time - " + timeDifference + " seconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
