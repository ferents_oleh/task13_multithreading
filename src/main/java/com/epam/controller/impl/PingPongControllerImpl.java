package com.epam.controller.impl;

import com.epam.controller.PingPongController;
import com.epam.service.PingPongTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPongControllerImpl implements PingPongController {
    @Override
    public void startPingPong() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Lock lock = new ReentrantLock();
        for (int i = 1; i <= 5; ++i) {
            executorService.submit(new PingPongTask("ping", lock));
            executorService.submit(new PingPongTask("pong", lock));
        }
    }
}
