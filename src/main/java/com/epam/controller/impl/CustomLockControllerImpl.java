package com.epam.controller.impl;

import com.epam.controller.CustomLockController;
import com.epam.service.CustomLock;
import com.epam.service.CustomLockTask;

public class CustomLockControllerImpl implements CustomLockController {
    private CustomLock customLock;

    public CustomLockControllerImpl(CustomLock customLock) {
        this.customLock = customLock;
    }

    @Override
    public void useCustomLock() {
        new Thread(new CustomLockTask(customLock), "First").start();
        new Thread(new CustomLockTask(customLock), "Second").start();
    }
}
